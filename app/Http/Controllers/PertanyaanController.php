<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PertanyaanController extends Controller
{
    public function index () {
        $pertanyaan = DB::table('pertanyaan')->get();
        return view('home', compact('pertanyaan'));
    }

    public function create () {
        return view('form-tanya');
    }

    public function store (Request $request) {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required'
        ]);
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
            "tanggal_dibuat" => now(),
            "tanggal_diperbaharui" => now()
        ]);
        return redirect('/pertanyaan');
    }

    public function show ($id) {
        $show_pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('show', compact('show_pertanyaan'));
    }

    public function edit ($id) {
        $edit_pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('edit', compact('edit_pertanyaan'));
    }

    public function update ($id, Request $request) {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);

        $query = DB::table('pertanyaan')
            ->where('id', $id)
            ->update([
                'judul' => $request["judul"],
                'isi' => $request["isi"]
            ]);
        return redirect('/pertanyaan');
    }

    public function destroy ($id) {
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan');
    }
}
