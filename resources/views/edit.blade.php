@extends('template.template')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Halaman Update</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Update</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
  
      <!-- Main content -->
      <section class="content">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Silahkan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="/pertanyaan/{{$edit_pertanyaan->id}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" name="judul" id="judul" value="{{$edit_pertanyaan->judul}}" placeholder="Masukkan Judul">
                        @error('judul')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="isi">Isi</label>
                        <input type="text" class="form-control" name="isi" id="isi" value="{{$edit_pertanyaan->isi}}" placeholder="Masukkan Isi">
                        @error('isi')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Lakukan editing</button>
                </form>
            </div>
            <!-- /.card-body -->
          </div>
      </section>
      <!-- /.content -->
@endsection