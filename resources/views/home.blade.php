@extends('template.template')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Halaman Utama</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Index</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
  
      <!-- Main content -->
      <section class="content">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Isilah pertanyaan pada link berikut <a href="/pertanyaan/create">ini</a>.</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Judul</th>
                  <th>Isi</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                    @forelse ($pertanyaan as $key=>$value)
                        <tr>
                            <td>{{$key + 1}}</th>
                            <td>{{$value->judul}}</td>
                            <td>{{$value->isi}}</td>
                            <td>
                                <a href="/pertanyaan/{{$value->id}}" class="btn btn-info">Show</a>
                                <a href="/pertanyaan/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                                <form action="/pertanyaan/{{$value->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger my-1" value="Delete">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr colspan="3">
                            <td>No data</td>
                        </tr>  
                    @endforelse  
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
      </section>
      <!-- /.content -->
@endsection

@push('script')
  <script src="../../AdminLTE-3.1.0-rc/plugins/datatables/jquery.dataTables.js"></script>
  <script src="../../AdminLTE-3.1.0-rc/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>
@endpush