@extends('template.template')

@section('content')
    <h2>Show Post {{$show_pertanyaan->id}}</h2>
    <h4>{{$show_pertanyaan->judul}}</h4>
    <p>{{$show_pertanyaan->isi}}</p>
    <hr>
    <p>Tekan <a href="/pertanyaan">kembali</a> kalau mau</p>
@endsection