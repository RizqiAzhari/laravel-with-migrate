@extends('template.template')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Halaman Utama</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Create</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
  
      <!-- Main content -->
      <section class="content">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Silahkan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="/pertanyaan" method="post">
                    @csrf
                    <div class="input-group mb-3">
                      <input type="text" class="form-control" name="judul" value="{{ old('judul', '') }}" placeholder="Judul">
                        @error('judul')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="input-group mb-3">
                      <textarea class="form-control" name="isi" value="{{ old('isi', '') }}" placeholder="Isi"></textarea>
                        @error('isi')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                      <!-- /.col -->
                      <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block">Ajukan Pertanyaan</button>
                      </div>
                      <!-- /.col -->
                    </div>
                  </form>
            </div>
            <!-- /.card-body -->
          </div>
      </section>
      <!-- /.content -->
@endsection